# Start with ruby version 2.2.3
FROM ruby:2.2.3

# Update apt quietly and install essential libraries
RUN apt-get update -qq && apt-get install -y build-essential

# Install libraries for PostgreSQL
RUN apt-get install -y libpq-dev

# for nokogiri
RUN apt-get install -y libxml2-dev libxslt1-dev

# for capybara-webkit
RUN apt-get install -y libqt4-webkit libqt4-dev xvfb

# for node
RUN curl -sL https://deb.nodesource.com/setup_0.12 | bash - && apt-get install -y nodejs

# Create the application directory and set it as the working directory
ENV APP_HOME /npm_demo
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# Add Gemfile(.lock)
ADD Gemfile* $APP_HOME/

# Bundler configuration
ENV BUNDLE_GEMFILE=$APP_HOME/Gemfile
ENV BUNDLE_JOBS=2
ENV BUNDLE_PATH=/bundle

# Add the current directory to the app
ADD . $APP_HOME
